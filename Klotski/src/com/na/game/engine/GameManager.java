package com.na.game.engine;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.na.game.engine.event.Updater;
import com.na.game.engine.resource.Destruction;
import com.na.game.engine.util.GameRuntimeException;

public class GameManager {

	private static Map<Class, Clazz> map;
	
	public static void init(Clazz... objs) {
		map = new HashMap<Class, Clazz>();
		for (Clazz o : objs) {
			map.put(o.getClazz(), o);
		}
	}
	
	public static void add(Clazz obj) {
		if (map == null) {
			throw new GameRuntimeException("GameManager's map null.");
		}
		map.put(obj.getClazz(), obj);
	}
	
	public static <T> T get(Class<T> clazz) {
		return (T) map.get(clazz);
	}
	
	public static void update() {
		Collection<Clazz> values = map.values();
		for (Clazz o : values) {
			if (o instanceof Updater) {
				((Updater) o).update();
			}
		}
	}
	
	public static void destroy () {
		Collection<Clazz> values = map.values();
		for (Clazz o : values) {
			if (o instanceof Destruction) {
				((Destruction) o).destroy();
			}
		}
		map.clear();
		map = null;
	}
	
	public static interface Clazz {
		public Class<?> getClazz();
	}
}
