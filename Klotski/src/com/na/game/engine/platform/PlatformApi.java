package com.na.game.engine.platform;

public abstract class PlatformApi {

	public static final int CMD_INIT = 0;
	public static final int CMD_DESTROY = 1;
	public static final int CMD_SHOW_BANNER = 2;
	public static final int CMD_HIDE_BANNER = 3;
	public static final int CMD_SHOW_POP_AD = 4;
	public static final int CMD_SHOW_MINI_AD = 5;
	
	private static PlatformApi ref;
	
	public static void init(PlatformApi inst) {
		ref = inst;
	}
	
	public static void invoke(int cmd, Object... params) {
		ref.invokeImpl(cmd, params);
	}
	
	protected abstract void invokeImpl(int cmd, Object... params);
	
}
