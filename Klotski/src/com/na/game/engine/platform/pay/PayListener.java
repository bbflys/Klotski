package com.na.game.engine.platform.pay;

public interface PayListener {
	int SUCCESS = 0;
	public void onPayFinish(String orderId, int resultCode, String resultString, float money);
}
