//package com.na.game.engine.platform.ad;
//
//import android.view.Gravity;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import cn.android.vip.feng.ui.AppControl;
//
//import com.na.game.engine.GameActivity;
//import com.na.game.engine.GameManager;
//import com.na.game.engine.platform.PlatformApi;
//
//public class Mobile7 extends PlatformApi {
//
//	private GameActivity ctx;
//	private View banner;
//	
//	@Override
//	protected void invokeImpl(int cmd, Object... params) {
//		switch (cmd) {
//			case CMD_INIT:
//				init();
//				break;
//			case CMD_DESTROY:
//				break;
//			case CMD_SHOW_BANNER:
//				showBanner();
//				break;
//			case CMD_HIDE_BANNER:
//				break;
//			case CMD_SHOW_POP_AD:
//				showPopAd();
//				break;
//			case CMD_SHOW_MINI_AD:
//				break;
//		}
//	}
//	
//	private void init() {
//		final GameActivity ctx = GameManager.get(GameActivity.class);
//		this.ctx = ctx;
//		AppControl.getInstance().loadPopAd(ctx);
//	}
//	
//	private void showBanner() {
//		ctx.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				if (banner != null) {
//					ctx.getContentView().removeView(banner);
//				}
//				LinearLayout adlayout = new LinearLayout(ctx); 
//				adlayout.setGravity(Gravity.CENTER_HORIZONTAL); 
//				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT); 
//				layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//				ctx.getContentView().addView(adlayout, layoutParams);
//				banner = adlayout;
//				AppControl.getInstance().showInter(ctx, adlayout);
//			}
//		});
//	}
//	
//	private void showPopAd() {
//		ctx.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				AppControl.getInstance().showPopAd(ctx, 10 * 1000);
//			}
//		});
//	}
//
//}
