//package com.na.game.engine.platform.ad;
//
//import android.widget.RelativeLayout;
//import android.widget.RelativeLayout.LayoutParams;
//
//import com.na.game.engine.GameActivity;
//import com.na.game.engine.GameManager;
//import com.na.game.engine.platform.PlatformApi;
//import com.na.game.engine.util.Util;
//import com.qq.e.ads.AdListener;
//import com.qq.e.ads.AdRequest;
//import com.qq.e.ads.AdSize;
//import com.qq.e.ads.AdView;
//import com.qq.e.ads.InterstitialAd;
//import com.qq.e.ads.InterstitialAdListener;
//
///**
// * 广点通
// * 
// * @author Ocoao
// * 
// */
//public class GuangDianTongApi extends PlatformApi {
//
//	private GameActivity ctx;
//	private String APP_ID;
//	private String BANNER_ID;
//	private String POPAD_ID;
//	
//	@Override
//	protected void invokeImpl(int cmd, Object... params) {
//		switch (cmd) {
//			case CMD_INIT:
//				init();
//				break;
//			case CMD_DESTROY:
//				break;
//			case CMD_SHOW_BANNER:
//				showBanner();
//				break;
//			case CMD_HIDE_BANNER:
//				break;
//			case CMD_SHOW_POP_AD:
//				showPopAd();
//				break;
//			case CMD_SHOW_MINI_AD:
//				break;
//		}
//	}
//	
//	private void init() {
//		ctx = GameManager.get(GameActivity.class);
//		APP_ID = String.valueOf(Util.getAppMetaInt(ctx, "APP_ID"));
//		BANNER_ID = String.valueOf(Util.getAppMetaInt(ctx, "BANNER_ID"));
//		POPAD_ID = String.valueOf(Util.getAppMetaInt(ctx, "POPAD_ID"));
//	}
//
//	private void showBanner() {
//		ctx.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				AdView adv = new AdView(ctx, AdSize.BANNER, APP_ID, BANNER_ID);
//				LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//				lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//				ctx.getContentView().addView(adv, lp);
//				
//				AdRequest adr = new AdRequest(); 
//				adr.setRefresh(30); 
//				adr.setShowCloseBtn(false); 
//				adv.setAdListener(new AdListener() {
//					@Override
//					public void onNoAd() {
//					}
//					@Override
//					public void onBannerClosed() {
//					}
//					@Override
//					public void onAdReceiv() {
//					}
//					@Override
//					public void onAdExposure() {
//					}
//					@Override
//					public void onAdClicked() {
//					}
//				}); 
//				adv.fetchAd(adr);
//			}
//		});
//	}
//	
//	private void showPopAd() {
//		InterstitialAd iad = new InterstitialAd(ctx, APP_ID, POPAD_ID);
//		iad.setAdListener(new InterstitialAdListener() {
//			@Override
//			public void onFail() {
//			}
//			@Override
//			public void onExposure() {
//			}
//			@Override
//			public void onClicked() {
//			}
//			@Override
//			public void onBack() {
//			}
//			@Override
//			public void onAdReceive() {
//			}
//		});
//		iad.loadAd();
//		iad.show();
//	}
//}
