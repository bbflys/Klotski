package com.na.game.engine.platform.ad;

import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mediav.ads.sdk.adcore.MediavSimpleAds;
import com.mediav.ads.sdk.adcore.MvBannerAd;
import com.mediav.ads.sdk.adcore.MvInterstitialAd;
import com.na.game.engine.GameActivity;
import com.na.game.engine.GameManager;
import com.na.game.engine.platform.PlatformApi;
import com.na.game.engine.util.Util;

/**
 * 360聚效
 * @author Ocoao
 *
 */
public class Mediav extends PlatformApi {

	private GameActivity ctx;
	private static String BANNER_ID;
	private static String POP_ID;
	
	private LinearLayout banner;
	
	@Override
	protected void invokeImpl(int cmd, Object... params) {
		switch (cmd) {
			case CMD_INIT:
				init();
				break;
			case CMD_DESTROY:
				destroy();
				break;
			case CMD_SHOW_BANNER:
				showBanner();
				break;
			case CMD_HIDE_BANNER:
				break;
			case CMD_SHOW_POP_AD:
				showPopAd();
				break;
			case CMD_SHOW_MINI_AD:
				break;
		}
	}
	
	private void init() {
		ctx = GameManager.get(GameActivity.class);
		BANNER_ID = Util.getAppMetaString(ctx, "BANNER_ID");
		POP_ID = Util.getAppMetaString(ctx, "POP_ID");
	}
	
	private void showBanner() {
		ctx.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (banner != null) {
					ctx.getContentView().removeView(banner);
				}
				LinearLayout adLayout = new LinearLayout(ctx);
				adLayout.setGravity(Gravity.CENTER_HORIZONTAL);
				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT); 
				layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				ctx.getContentView().addView(adLayout, layoutParams);
				banner = adLayout;
				MvBannerAd ad = MediavSimpleAds.initSimpleBanner(adLayout, ctx, BANNER_ID, false);
				ad.showAds(ctx);
			}
		});
	}
	
	private void showPopAd() {
		ctx.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				MvInterstitialAd ad = MediavSimpleAds.initSimpleInterstitial(ctx, POP_ID, false);
				MediavSimpleAds.reloadInterstitial(ctx);
				ad.showAds(ctx);
			}
		});
	}

	private void destroy() {
		MediavSimpleAds.unregisterMediavReceiver(ctx);
	}
}
