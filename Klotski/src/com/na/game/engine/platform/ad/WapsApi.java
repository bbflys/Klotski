//package com.na.game.engine.platform.ad;
//
//import android.view.Gravity;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import cn.waps.AppConnect;
//
//import com.na.game.engine.GameActivity;
//import com.na.game.engine.GameManager;
//import com.na.game.engine.platform.PlatformApi;
//
//public class WapsApi extends PlatformApi {
//
//	private GameActivity ctx;
//	private LinearLayout banner;
//	
//	@Override
//	protected void invokeImpl(int cmd, Object... params) {
//		switch (cmd) {
//			case CMD_INIT:
//				init();
//				break;
//			case CMD_DESTROY:
//				close();
//				break;
//			case CMD_SHOW_BANNER:
//				showBanner();
//				break;
//			case CMD_HIDE_BANNER:
//				hideBanner();
//				break;
//			case CMD_SHOW_POP_AD:
//				showPopAd();
//				break;
//			case CMD_SHOW_MINI_AD:
//				showMini();
//				break;
//		}
//	}
//	
//	private void init() {
//		GameActivity ctx = GameManager.get(GameActivity.class);
//		this.ctx = ctx;
//		AppConnect.getInstance(ctx);
//		AppConnect.getInstance(ctx).initPopAd(ctx);
//	}
//	
//	private void close() {
//		AppConnect.getInstance(GameManager.get(GameActivity.class)).close();
//	}
//	
//	private void showBanner() {
//		ctx.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				if (banner != null) {
//					ctx.getContentView().removeView(banner);
//				}
//				LinearLayout adlayout = new LinearLayout(ctx); 
//				adlayout.setGravity(Gravity.CENTER_HORIZONTAL); 
//				RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT); 
//				AppConnect.getInstance(ctx).showBannerAd(ctx, adlayout); 
//				layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//				ctx.getContentView().addView(adlayout, layoutParams);
//				banner = adlayout;
//			}
//		});
//	}
//	
//	private void hideBanner() {
//		if (banner != null) {
//			banner.setVisibility(View.INVISIBLE);
//		}
//	}
//	
//	private void showPopAd() {
//		AppConnect.getInstance(ctx).showPopAd(ctx); 
//	}
//	
//	private void showMini() {}
//
//}
