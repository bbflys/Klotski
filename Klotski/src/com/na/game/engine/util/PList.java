package com.na.game.engine.util;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Rect;

import com.na.game.engine.graphics.Image;

public class PList {

	private List<PList.PListItem> items = new ArrayList<PList.PListItem>();
	private Image image;
	
	public PList(Image image) {
		this(10, image);
	}
	
	public PList(int capacity, Image image) {
		items = new ArrayList<PList.PListItem>(capacity);
		this.image = image;
	}
	
	public void addItem(PListItem item) {
		items.add(item);
	}
	
	public PListItem getItem(int index) {
		return items.get(index);
	}
	
	public Image getImage() {
		return image;
	}
	
	public int size() {
		return items.size();
	}
	
	public static class PListItem {
		public int index;
		public Rect bound;
		public PListItem(int index, Rect bound) {
			this.index = index;
			this.bound = bound;
		}
	}
}
