package com.na.game.engine.util;

public class GameRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -1723332239722563019L;
	
	public GameRuntimeException() {
		super();
	}
	
	public GameRuntimeException(String msg) {
		super(msg);
	}
	
	public GameRuntimeException(Throwable t) {
		super(t);
	}

}
