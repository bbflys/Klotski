package com.na.game.engine.util;

public class GameException extends Exception {

	private static final long serialVersionUID = -7596231976282278064L;

	public GameException() {
		super();
	}
	
	public GameException(String msg) {
		super(msg);
	}
	
	public GameException(Throwable t) {
		super(t);
	}
	
}
