package com.na.game.engine.util;

import java.util.Vector;

public class Timer {

	private Vector<TimerTask> tasks = new Vector<TimerTask>();
	
	public void schedule(TimerTask task, long delay) {
		schedule(task, delay, 0);
	}

	public void schedule(TimerTask task, long delay, long period) {
		tasks.addElement(task);
		task.schedule(this, delay, period);
	}

	public void cancel() {
		for (TimerTask task : tasks) {
			task.cancel();
		}
		tasks.removeAllElements();
	}
	
	public void cancel(TimerTask task) {
		if (tasks.removeElement(task)) {
			task.cancel();
		}
	}
	
	public void cancel(int index) {
		if (index >= 0 && index < tasks.size()) {
			tasks.remove(index).cancel();
		}
	}
	
	protected void remove(TimerTask task) {
		tasks.removeElement(task);
	}
}
