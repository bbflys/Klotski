package com.na.game.engine.util;

import java.io.File;

import android.graphics.Bitmap;

public class GameConfig {
	public static int MSPF = 1000 / 60; // millisecond per frame
	// iphone5
	public static int STANDARD_WIDTH = 640;//720;
	public static int STANDARD_HEIGHT = 1136;//1280;
	public static int FONT_DEFAULT_SIZE = 50;
	
	@Deprecated
	public static final String RES_PROPS_XML = "props/props.xml";
	public static final String RES_GAME_DATA = "com.nagame.klotski/na_game.dta";
	public static final String AUTO_DEVICE_ID = "device_id";
	
	public static int DENSITY = Bitmap.DENSITY_NONE;
	public static File SDCARD = null;
	public static int screenWidth, screenHeight;
	public static boolean useOpenGL;
	public static final boolean DEBUG = true;
}
