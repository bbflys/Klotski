package com.na.game.engine.util;

import java.util.HashMap;
import java.util.Map;

public class Cache {

	private static Cache cache;
	private Map<String, Object> data;
	
	private Cache() {
		data = new HashMap<String, Object>();
	}
	
	public static Cache getCache() {
		if (cache == null) {
			cache = new Cache();
		}
		return cache;
	}
	
	public Object get(String key) {
		return data.get(key);
	}
	
	public void put(String key, Object value) {
		data.put(key, value);
	}
	
	public void remove(String key) {
		data.remove(key);
	}
	
	public static void destroy() {
		if (cache != null) {
			cache.data.clear();
			cache = null;
		}
	}
	
}
