package com.na.game.engine.resource;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Typeface;

import com.na.game.engine.GameActivity;
import com.na.game.engine.GameManager;
import com.na.game.engine.GameManager.Clazz;
import com.na.game.engine.graphics.Image;
import com.na.game.engine.opengl.GLImage;
import com.na.game.engine.util.GameConfig;
import com.na.game.engine.util.Util;

public class ResourceManager implements Destruction, Clazz {

	private Map<String, Reference<Image>> imageCache = new HashMap<String, Reference<Image>>();
	private Map<String, Typeface> typefaceCache = new HashMap<String, Typeface>();
	
	public InputStream getAssets(String fileName) {
		synchronized (fileName) {
			return Util.getAssets(fileName);
		}
	}
	
	public Image getImageFromAssets(String fileName) {
		synchronized (fileName) {
			Reference<Image> ref = imageCache.get(fileName);
			Image image = ref != null ? ref.get() : null;
			if (image != null && !image.isRecycled()) {
				return image;
			}
			InputStream in = getAssets(fileName);
			if (in != null) {
				Options op = new Options();
				op.inPreferredConfig = Config.ARGB_8888;
				Bitmap bitmap = BitmapFactory.decodeStream(in, null, op);
				image = GameConfig.useOpenGL ? new GLImage(bitmap, fileName) : new Image(bitmap, fileName);
				try {
					in.close();
				} catch (IOException e) {
				}
				imageCache.put(fileName, new SoftReference<Image>(image));
				return image;
			}
			return null;
		}
	}
	
	public Typeface getTypefaceFromAssets(String fileName) {
		Typeface typeface = typefaceCache.get(fileName);
		if (typeface != null) {
			return typeface;
		}
		typeface = Typeface.createFromAsset(GameManager.get(GameActivity.class).getAssets(), fileName);
		typefaceCache.put(fileName, typeface);
		return typeface;
	}
	
	@Override
	public void destroy() {
		Collection<Reference<Image>> refs = imageCache.values();
		for (Reference<Image> ref : refs) {
			Image image = ref.get();
			if (image != null) {
				image.destroy();
			}
		}
		imageCache.clear();
	}

	@Override
	public Class<?> getClazz() {
		return getClass();
	}
	
}
