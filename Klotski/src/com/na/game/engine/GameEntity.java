package com.na.game.engine;

import android.content.Intent;
import android.graphics.Typeface;

import com.na.game.engine.ui.Resolution;
import com.na.game.engine.ui.UI;

public abstract class GameEntity {

	protected GameActivity context;
	public GameEntity(GameActivity context) {
		this.context = context;
	}
	
	public Typeface getDefaultFont() {
		return Typeface.DEFAULT;
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	}
	
	public abstract UI getFirstUI();
	public abstract Resolution getResolution();
}