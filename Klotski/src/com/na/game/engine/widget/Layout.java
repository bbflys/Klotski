package com.na.game.engine.widget;

import com.na.game.engine.graphics.Graphics;

public class Layout {
	public static final int TOP_LEFT = Graphics.TOP | Graphics.LEFT;
	public static final int TOP_RIGHT = Graphics.TOP | Graphics.RIGHT;
	public static final int BOTTOM_LEFT = Graphics.BOTTOM | Graphics.LEFT;
	public static final int BOTTOM_RIGHT = Graphics.BOTTOM | Graphics.RIGHT;
	public static final int LEFT_CENTER = Graphics.LEFT | Graphics.VCENTER;
	public static final int RIGHT_CENTER = Graphics.RIGHT | Graphics.VCENTER;
	public static final int TOP_CENTER = Graphics.TOP | Graphics.HCENTER;
	public static final int BOTTOM_CENTER = Graphics.BOTTOM | Graphics.HCENTER;
	public static final int CENTER = Graphics.HCENTER | Graphics.VCENTER;
}
