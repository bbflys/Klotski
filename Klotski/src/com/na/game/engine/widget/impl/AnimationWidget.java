package com.na.game.engine.widget.impl;

import com.na.game.engine.graphics.Animation;
import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.widget.Widget;

public class AnimationWidget extends Widget {

	protected Animation animation;
	
	public AnimationWidget(String assetsXml) {
		this(new Animation(assetsXml));
	}
	
	public AnimationWidget(Animation animation) {
		this.animation = animation;
	}
	
	@Override
	public void setBound(int x, int y, int width, int height) {
		super.setBound(x, y, width, height);
		animation.position(getAbsX(), getAbsY());
	}

	@Override
	protected void paintImpl(Graphics g) {
		if (visible) {
			animation.paint(g);
		}
	}

	@Override
	public void update() {
		super.update();
		if (visible) {
			animation.update();
		}
	}
	
}
