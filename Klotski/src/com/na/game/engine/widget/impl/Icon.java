package com.na.game.engine.widget.impl;

import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.graphics.Image;
import com.na.game.engine.widget.Layout;
import com.na.game.engine.widget.Widget;

public class Icon extends Widget {

	protected Image image;
	protected int imageColor = Graphics.COLOR_FILTER_NULL;
	
	public Icon(Image image) {
		this.image = image;
	}
	
	public void setIcon(Image image) {
		this.image = image;
	}
	
	public void setImageColor(int color) {
		imageColor = color;
	}

	@Override
	protected void paintImpl(Graphics g) {
		if (image != null) {
			g.setColorFilter(imageColor);
			if (scaled) {
				g.drawImage(image, src, absBound);
			} else {
				g.drawImage(image, absBound.left, absBound.top, Layout.TOP_LEFT);
			}
			g.setColorFilter(Graphics.COLOR_FILTER_NULL);
		}
	}
	
}
