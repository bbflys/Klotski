package com.na.game.engine.widget.impl;

import android.graphics.Typeface;

import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.util.GameConfig;
import com.na.game.engine.widget.Layout;
import com.na.game.engine.widget.Widget;

public class Label extends Widget {

	protected String text;
	protected boolean is3D;
	protected int color, borderColor;
	protected int textAlign = Layout.CENTER;
	protected int textSize;
	protected Typeface typeface;
	
	public Label(String text) {
		this(text, 0x000000);
	}
	
	public Label(String text, int color) {
		this(text, false, color, 0);
	}
	
	public Label(String text, boolean is3D, int color, int borderColor) {
		this(text, is3D, color, borderColor, GameConfig.FONT_DEFAULT_SIZE);
	}
	
	public Label(String text, boolean is3D, int color, int borderColor, int textSize) {
		this.text = text;
		this.is3D = is3D;
		this.color = color;
		this.borderColor = borderColor;
		this.textSize = textSize;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public void setTextAlign(int align) {
		textAlign = align;
	}
	
	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}
	
	public void setTypeface(Typeface typeface) {
		this.typeface = typeface;
	}
	
	public void setIs3D(boolean is3D) {
		this.is3D = is3D;
	}
	
	public void setTextColor(int color) {
		this.color = color;
	}
	
	public void setTextBorderColor(int borderColor) {
		this.borderColor = borderColor;
	}

	@Override
	protected void paintImpl(Graphics g) {
		if (text != null && text.length() > 0) {
			int x = absBound.left;
			int y = absBound.top;
			switch (textAlign) {
				case Layout.TOP_RIGHT:
					x = absBound.right;
					break;
				case Layout.BOTTOM_LEFT:
					y = absBound.bottom;
					break;
				case Layout.BOTTOM_RIGHT:
					x = absBound.right;
					y = absBound.bottom;
					break;
				case Layout.LEFT_CENTER:
					y += absBound.height() / 2;
					break;
				case Layout.RIGHT_CENTER:
					x = absBound.right;
					y += absBound.height() / 2;
					break;
				case Layout.TOP_CENTER:
					x += absBound.width() / 2;
					break;
				case Layout.BOTTOM_CENTER:
					x += absBound.width() / 2;
					y = absBound.bottom;
					break;
				case Layout.CENTER:
					x += absBound.width() / 2;
					y += absBound.height() / 2;
					break;
			}
			int oldTextSize = g.getTextSize();
			Typeface oldTypeface = g.getTypeface();
			g.setTextSize(textSize);
			if (typeface != null) {
				g.setTypeface(typeface);
			}
			if (is3D) {
				g.draw3DString(text, x, y, textAlign, color, borderColor);
			} else {
				g.setColor(color);
				g.drawString(text, x, y, textAlign);
			}
			g.setTextSize(oldTextSize);
			if (typeface != null) {
				g.setTypeface(oldTypeface);
			}
		}
	}
}
