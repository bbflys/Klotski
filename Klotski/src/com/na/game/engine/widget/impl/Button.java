package com.na.game.engine.widget.impl;

import android.graphics.NinePatch;

import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.graphics.Image;
import com.na.game.engine.widget.Layout;

public class Button extends Label {

	protected Image[] images;
	private NinePatch[] ninePatchs;
	private int imageColor = 0xFFFFFFFF;
	
	public Button(Image[] images) {
		this(images, null, 0);
	}
	
	public Button(Image[] images, String text, int color) {
		super(text, color);
		this.images = images;
		if (images != null) {
			ninePatchs = new NinePatch[images.length];
			for (int i = images.length - 1; i >= 0; i--) {
				if (images[i] == null) {
					continue;
				}
				byte[] chunk = images[i].getNinePatchChunk();
				if (chunk != null) {
					ninePatchs[i] = new NinePatch(images[i].getBitmap(), chunk, "");
				}
			}
		}
	}
	
	public Button(Image image) {
		this(new Image[]{image});
	}
	
	public Button(Image image, String text, int color) {
		this(new Image[]{image}, text, color);
	}
	
	public void setImage(Image image, int index) {
		images[index] = image;
	}
	
	public void setImageColor(int imageColor) {
		this.imageColor = imageColor;
	}
	
	@Override
	protected void paintImpl(Graphics g) {
		if (images != null) {
			if (status >= images.length) {
				status = 0;
			}
			g.setColorFilter(imageColor);
			if (ninePatchs[status] != null) {
				g.drawNinePatch(ninePatchs[status], absBound);
			} else if (scaled) {
				g.drawImage(images[status], src, absBound);
			} else {
				g.drawImage(images[status], absBound.left, absBound.top, Layout.TOP_LEFT);
			}
			g.setColorFilter(Graphics.COLOR_FILTER_NULL);
		}
		super.paintImpl(g);
	}
	
}
