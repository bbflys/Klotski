package com.na.game.engine.widget.impl;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Typeface;

import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.util.Util;
import com.na.game.engine.widget.Layout;


public class TextArea extends Label {

	private String[] textArr;
	private int charHeight;
	
	public TextArea(String text) {
		super(text);
	}
	
	public TextArea(String text, int color) {
		super(text, color);
	}

	@Override
	public void setBound(int x, int y, int width, int height) {
		super.setBound(x, y, width, height);
		splitText(width);
	}
	
	@Override
	public void setText(String text) {
		super.setText(text);
		splitText(getBound().width());
	}

	@Override
	public void setTextSize(int textSize) {
		super.setTextSize(textSize);
		splitText(getBound().width());
	}

	private void splitText(int width) {
		if (text == null || text.length() == 0) {
			return;
		}
		charHeight = Util.getTextHeight("高", textSize);
		int charWidth = getCharMaxWidth();
		int cols = width / charWidth;
		if (cols <= 0) {
			return;
		}
		int start = 0;
		List<String> arr = new ArrayList<String>();
		while (true) {
			int end = start + cols > text.length() ? text.length() : start + cols;
			int linePos = text.indexOf('\n', start);
			String str = text.substring(start, linePos >= 0 && linePos <= end ? linePos : end);
			arr.add(str);
			if (linePos >= 0 && linePos <= end) {
				end = linePos + 1;
			}
			if (end == text.length()) {
				break;
			}
			start = end;
		}
		textArr = arr.toArray(new String[arr.size()]);
	}
	
	private int getCharMaxWidth() {
		if (text == null) {
			return 0;
		}
		int w = 0;
		for (int i = text.length() - 1; i >= 0; i--) {
			int cw = Util.getTextWidth(String.valueOf(text.charAt(i)), textSize);
			if (cw > w) {
				w = cw;
			}
		}
		return w;
	}
	
	@Override
	protected void paintImpl(Graphics g) {
		g.setColor(0xFFFFFFFF);
		if (textArr != null && textArr.length > 0) {
			int x = absBound.left;
			int y = absBound.top;
			switch (textAlign) {
				case Layout.TOP_RIGHT:
					x = absBound.right;
					break;
				case Layout.BOTTOM_LEFT:
					y = absBound.bottom - (textArr.length - 1) * charHeight;
					break;
				case Layout.BOTTOM_RIGHT:
					x = absBound.right;
					y = absBound.bottom - (textArr.length - 1) * charHeight;
					break;
				case Layout.LEFT_CENTER:
					y += (absBound.height() - textArr.length * charHeight) / 2 + charHeight / 2;
					break;
				case Layout.RIGHT_CENTER:
					x = absBound.right;
					y += (absBound.height() - textArr.length * charHeight) / 2 + charHeight / 2;
					break;
				case Layout.TOP_CENTER:
					x += absBound.width() / 2;
					break;
				case Layout.BOTTOM_CENTER:
					x += absBound.width() / 2;
					y = absBound.bottom - (textArr.length - 1) * charHeight;
					break;
				case Layout.CENTER:
					x += absBound.width() / 2;
					y += (absBound.height() - textArr.length * charHeight) / 2 + charHeight / 2;
					break;
			}
			int oldTextSize = g.getTextSize();
			Typeface oldTypeface = g.getTypeface();
			g.setTextSize(textSize);
			if (typeface != null) {
				g.setTypeface(typeface);
			}
			for (String text : textArr) {
				if (is3D) {
					g.draw3DString(text, x, y, textAlign, color, borderColor);
				} else {
					g.setColor(color);
					g.drawString(text, x, y, textAlign);
				}
				y += charHeight;
			}
			g.setTextSize(oldTextSize);
			if (typeface != null) {
				g.setTypeface(oldTypeface);
			}
		}
	}
	
}
