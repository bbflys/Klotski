package com.na.game.engine.widget.impl;

import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.game.engine.event.impl.TouchEvent;
import com.na.game.engine.widget.Widget;

public class ScrollPanel extends Container {
	
	public static final int VERTICAL = 0;
	public static final int HORIZONTAL = 1;
	
	private int dir = VERTICAL;
	private int lastTouchPos;
	private int lastMoveDistance;
	private int min, max;
	private boolean fireUpdate, reset;

	public ScrollPanel() {
		this(VERTICAL);
	}
	
	public ScrollPanel(int dir) {
		super(1);
		this.dir = dir;
	}
	
	@Override
	public void addChild(Widget child) {
		if (children.size() == 1) {
			throw new ArrayIndexOutOfBoundsException("scrollpanel children == 1");
		}
		super.addChild(child);
		setContent(child);
	}

	public void setContent(Widget content) {
		if (content != null) {
			content.setParent(this);
			if (dir == VERTICAL) {
				min = content.getBound().top;
				max = bound.height();
			} else {
				min = content.getBound().left;
				max = bound.width();
			}
		}
	}

	@Override
	public boolean onTouch(TouchEvent event) {
		if (!visible) {
			return false;
		}
		if (children.size() > 0) {
			if (dir == VERTICAL) {
				switch (event.action) {
					case MotionEvent.ACTION_DOWN:
						resetChildBound();
						lastTouchPos = event.y;
						break;
					case MotionEvent.ACTION_MOVE:
						Widget content = children.elementAt(0);
						Rect last = content.getBound();
						if (last.top < min + 20 && last.bottom > max - 20) {
							lastMoveDistance = event.y - lastTouchPos;
							lastTouchPos = event.y;
							content.setBound(last.left, last.top + lastMoveDistance, last.width(), last.height());
						} else {
							lastMoveDistance = 0;
						}
						break;
					case MotionEvent.ACTION_UP:
					case MotionEvent.ACTION_OUTSIDE:
						fireUpdate = true;
						break;
				}
			}
		}
		return super.onTouch(event);
	}
	
	@Override
	public void update() {
		if (fireUpdate) {
			Widget content = children.elementAt(0);
			Rect last = content.getBound();
			if (last.top < min + 20 && last.bottom > max - 20) {
				if (lastMoveDistance == 0) {
					resetChildBound();
				} else {
					int distance = lastMoveDistance * 6;
					content.setBound(last.left, last.top + distance, last.width(), last.height());
					lastMoveDistance += lastMoveDistance > 0 ? -1 : 1;
				}
			} else {
				resetChildBound();
			}
		}/* else if (reset) {
			Widget content = children.elementAt(0);
			Rect last = content.getBound();
			if (last.top > min) {
				content.setBound(last.left, last.top - 4, last.width(), last.height());
			} else if (last.bottom < max) {
				content.setBound(last.left, last.top + 4, last.width(), last.height());
			} else {
				reset = false;
				if (last.top > min) {
					content.setBound(last.left, min, last.width(), last.height());
				} else if (last.bottom < max) {
					content.setBound(last.left, max - last.height(), last.width(), last.height());
				}
			}
		}*/
		super.update();
	}
	
	private void resetChildBound() {
		fireUpdate = false;
//		reset = true;
		Widget content = children.elementAt(0);
		Rect last = content.getBound();
		if (last.top > min) {
			content.setBound(last.left, min, last.width(), last.height());
		} else if (last.bottom < max) {
			content.setBound(last.left, max - last.height(), last.width(), last.height());
		}
	}
}
