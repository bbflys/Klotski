package com.na.game.engine.widget.impl;

import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.graphics.Image;
import com.na.game.engine.widget.Layout;
import com.na.game.engine.widget.Widget;

public class Grid extends Widget {

	protected int index; // in grid panel
	protected Image image;
	
	protected Grid() {}
	
	public Grid(Image image, int index) {
		this.image = image;
		this.index = index;
	}

	@Override
	protected void paintImpl(Graphics g) {
		if (scaled) {
			g.drawImage(image, src, absBound);
		} else {
			g.drawImage(image, absBound.left, absBound.top, Layout.TOP_LEFT);
		}
	}
}
