package com.na.game.engine.widget.impl;

import com.na.game.engine.widget.Widget;

public class GridPanel extends Container {

	private int rows, cols;
	private int hGap, vGap;
	
	public GridPanel(int rows, int cols) {
		super(rows * cols);
		this.rows = rows;
		this.cols = cols;
	}

	@Override
	public void addChild(Widget child) {
		if (children.size() < children.capacity()) {
			super.addChild(child);
		}
	}
	
	public void setGap(int hGap, int vGap) {
		this.hGap = hGap;
		this.vGap = vGap;
	}
	
	public void doLayout() {
		int gridWidth = (bound.width() - (cols + 1) * hGap) / cols;
		int gridHeight = (bound.height() - (rows + 1) * vGap) / rows;
		for (int i = 0; i < children.size(); i++) {
			int x = hGap + i % cols * (gridWidth + hGap);
			int y = vGap + i / cols * (gridHeight + vGap);
			children.elementAt(i).setBound(x, y, gridWidth, gridHeight);
		}
	}
	
	public int size() {
		return children.capacity();
	}
	
}
