package com.na.game.engine.widget.impl;

import java.util.Vector;

import com.na.game.engine.event.impl.KeyEvent;
import com.na.game.engine.event.impl.TouchEvent;
import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.widget.Widget;

public class Container extends Widget {

	protected Vector<Widget> children;
	
	public Container() {
		this(10);
	}
	
	public Container(int size) {
		children = new Vector<Widget>(size);
	}
	
	public void addChild(Widget child) {
		children.add(child);
		child.setParent(this);
		child.setVisible(visible);
	}
	
	@Override
	protected void paintImpl(Graphics g) {
		int size = children.size();
		for (int i = 0; i < size; i++) {
			children.elementAt(i).paint(g);
		}
	}

	@Override
	public boolean onKey(KeyEvent event) {
		return false;
	}

	@Override
	public boolean onTouch(TouchEvent event) {
		for (int i = children.size() - 1; i >= 0; i--) {
			Widget child = children.elementAt(i);
			if (child.getAbsBound().contains(event.x, event.y)
					&& child.onTouch(event)) {
				return true;
			}
		}
		return super.onTouch(event);
	}

	@Override
	public void update() {
		for (Widget child : children) {
			child.update();
		}
		super.update();
	}

	@Override
	public void setBound(int x, int y, int width, int height) {
		super.setBound(x, y, width, height);
		for (Widget child : children) {
			child.setDirty();
		}
	}
	
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		for (Widget child : children) {
			child.setVisible(visible);
		}
	}
}
