package com.na.game.engine.ui;

public enum Resolution {
	SHOW_ALL, NO_BODER, FIXED_WIDTH, FIXED_HEIGHT
}
