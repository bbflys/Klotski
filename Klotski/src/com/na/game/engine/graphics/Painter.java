package com.na.game.engine.graphics;

public interface Painter {
	public void paint(Graphics g);
}
