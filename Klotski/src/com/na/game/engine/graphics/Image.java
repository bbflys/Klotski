package com.na.game.engine.graphics;

import android.graphics.Bitmap;

import com.na.game.engine.resource.Destruction;
import com.na.game.engine.util.GameConfig;

public class Image implements Destruction {

	protected Bitmap bitmap;
	protected String name; // debug
	protected byte[] chunk; // cache nine patch chunk
	
	public Image(Bitmap bitmap, String name) {
		this.bitmap = bitmap;
		if (GameConfig.DEBUG)
			this.name = name;
	}

	/**
	 * opengl: will be null after binded
	 * @return
	 */
	public Bitmap getBitmap() {
		return bitmap;
	}
	
	public byte[] getNinePatchChunk() {
		if (bitmap != null && chunk == null) {
			chunk = bitmap.getNinePatchChunk();
		}
		return chunk;
	}
	
	public int getWidth() {
		return bitmap != null ? bitmap.getWidth() : 0;
	}
	
	public int getHeight() {
		return bitmap != null ? bitmap.getHeight() : 0;
	}
	
	@Override
	public void destroy() {
		if (bitmap != null && !bitmap.isRecycled()) {
			bitmap.recycle();
			bitmap = null;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isRecycled() {
		if (bitmap != null) {
			return bitmap.isRecycled();
		}
		return true;
	}

	@Override
	public String toString() {
		return name + "{" + bitmap + "}";
	}
	
}
