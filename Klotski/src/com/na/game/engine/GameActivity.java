package com.na.game.engine;

import java.lang.reflect.Constructor;

import android.app.Activity;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.na.game.engine.GameManager.Clazz;
import com.na.game.engine.opengl.GLGameMain;
import com.na.game.engine.util.GameConfig;
import com.na.game.engine.util.Util;
import com.na.game.klotski.R;

public class GameActivity extends Activity implements Clazz {

	private GameEntity game;
	private RelativeLayout contentView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			String entityClass = Util.getAppMetaString(this, "game_entity_class");

			Class clazz = Class.forName(entityClass);
			Constructor con = clazz.getConstructor(GameActivity.class);
			game = (GameEntity) con.newInstance(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		boolean useOpenGL = Util.getAppMetaBoolean(this, "useOpenGL");
		GameConfig.useOpenGL = useOpenGL;
		SurfaceView view;
		if (useOpenGL) {
			view = createOpenGLView();
		} else {
			view = createView();
		}
		view.setId(10000);
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		view.requestFocus();
		
		RelativeLayout rl = (RelativeLayout) getLayoutInflater().inflate(R.layout.game, null);
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		lp.addRule(RelativeLayout.CENTER_IN_PARENT);
		rl.addView(view, lp);
		contentView = rl;
		
		setContentView(rl);
	}
	
	public RelativeLayout getContentView() {
		return contentView;
	}
	
	private SurfaceView createView() {
		SurfaceView view = new SurfaceView(this);
		GameMain gm = new GameMain(this, view);
		view.setOnTouchListener(gm);
		view.setOnKeyListener(gm);
		view.getHolder().addCallback(new SurfaceHolderCallback(gm));
		return view;
	}
	
	private SurfaceView createOpenGLView() {
		GLSurfaceView view = new GLSurfaceView(this);
		GLGameMain gm = new GLGameMain(this, view);
		view.setOnTouchListener(gm);
		view.setOnKeyListener(gm);
		view.setRenderer(gm);
		view.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		return view;
	}
	
	public GameEntity getGameEntity() {
		return game;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		game.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public Class<?> getClazz() {
		return getClass();
	}
	
	private class SurfaceHolderCallback implements Callback {

		private GameMain main;
		
		private SurfaceHolderCallback(GameMain main) {
			this.main = main;
		}
		
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			main.start();
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
		}
		
	}
}
