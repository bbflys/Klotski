package com.na.game.engine;

import android.content.Context;
import android.graphics.Canvas;
import android.media.AudioManager;
import android.os.Environment;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;

import com.na.game.engine.GameManager.Clazz;
import com.na.game.engine.data.DataManager;
import com.na.game.engine.event.Event;
import com.na.game.engine.event.EventManager;
import com.na.game.engine.event.impl.KeyEvent;
import com.na.game.engine.event.impl.TouchEvent;
import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.platform.PlatformApi;
import com.na.game.engine.resource.Destruction;
import com.na.game.engine.resource.ResourceManager;
import com.na.game.engine.sound.SoundManager;
import com.na.game.engine.ui.Resolution;
import com.na.game.engine.ui.UI;
import com.na.game.engine.ui.UIManager;
import com.na.game.engine.util.Cache;
import com.na.game.engine.util.GameConfig;
import com.na.game.engine.util.PListManager;
import com.na.game.engine.util.Util;

public class GameMain implements Runnable, OnTouchListener, OnKeyListener, Clazz, Destruction {

	protected boolean running;
	protected Graphics graphics;
	protected float scale = 1.0f;
	protected int screenWidth, screenHeight;
	protected float paintOffsetX, paintOffsetY;
	protected GameActivity context;
	protected AudioManager am;
	protected SurfaceView view;
	
	public GameMain(GameActivity context, SurfaceView view) {
		this.context = context;
		this.view = view;
		Display display = context.getWindowManager().getDefaultDisplay();
		screenWidth = display.getWidth();
		screenHeight = display.getHeight();
		if (screenWidth != GameConfig.STANDARD_WIDTH || screenHeight != GameConfig.STANDARD_HEIGHT) {
			calcScale();
		}
		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			GameConfig.SDCARD = Environment.getExternalStorageDirectory();
		}
		GameConfig.DENSITY = context.getResources().getDisplayMetrics().densityDpi;
		am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		GameManager.init(new EventManager(), new UIManager(), new ResourceManager(),
						context, new PListManager(), new SoundManager(context),
						/*new PropsManager(context),*/ new DataManager(context), this);
	}

	@Override
	public void run() {
		while (running) {
			long cost = System.currentTimeMillis();
			// main
			GameManager.update();
			repaint();
			// main
			cost = System.currentTimeMillis() - cost;
			if (cost < GameConfig.MSPF) {
				try {
					Thread.sleep(GameConfig.MSPF - cost);
				} catch (InterruptedException e) {
				}
			}
		}
		GameManager.destroy();
		context.finish();
	}
	
	protected void repaint() {
		if (graphics == null) {
			graphics = new Graphics(GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT, context.getGameEntity().getDefaultFont());
		}
		GameManager.get(UIManager.class).paint(graphics);
		
		SurfaceHolder holder = view.getHolder();
		Canvas canvas = holder.lockCanvas();
		if (canvas != null) {
			graphics.drawBuffer(canvas, paintOffsetX, paintOffsetY, scale);
			holder.unlockCanvasAndPost(canvas);
		}
	}
	
	@Override
	public boolean onKey(View v, int keyCode, android.view.KeyEvent event) {
		if (event.getAction() == android.view.KeyEvent.ACTION_DOWN) {
			switch (keyCode) {
				case android.view.KeyEvent.KEYCODE_VOLUME_UP:
					am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI | AudioManager.FLAG_PLAY_SOUND);
					break;
				case android.view.KeyEvent.KEYCODE_VOLUME_DOWN:
					am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI | AudioManager.FLAG_PLAY_SOUND);
					break;
			}
		}
		GameManager.get(EventManager.class).addEvent(new KeyEvent(Event.KEY_EVENT, event.getAction(), keyCode));
		return true;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		if (action == MotionEvent.ACTION_CANCEL) {
			action = MotionEvent.ACTION_UP;
		}
		GameManager.get(EventManager.class).addEvent(new TouchEvent(Event.TOUCH_EVENT, action, (event.getX() - paintOffsetX) / scale, (event.getY() - paintOffsetY) / scale));
		return true;
	}
	
	public void start() {
		if (!running) {
			running = true;
			UI ui = context.getGameEntity().getFirstUI();
			GameManager.get(UIManager.class).pushUI(ui);
			new Thread(this).start();
		}
	}
	
	public void stop() {
		running = false;
	}
	
	@Override
	public Class<?> getClazz() {
		return GameMain.class; // use GameMain.class
	}

	@Override
	public void destroy() {
		PlatformApi.invoke(PlatformApi.CMD_DESTROY);
		Cache.destroy();
		Util.destroy();
	}
	
	private void calcScale() {
		float xscale = (float) screenWidth / GameConfig.STANDARD_WIDTH;
		float yscale = (float) screenHeight / GameConfig.STANDARD_HEIGHT;
		Resolution rsl = context.getGameEntity().getResolution();
		switch (rsl) {
			case SHOW_ALL:
				scale = Math.min(xscale, yscale);
				break;
			case NO_BODER:
				scale = Math.max(xscale, yscale);
				break;
			case FIXED_WIDTH:
				scale = xscale;
				GameConfig.STANDARD_HEIGHT = (int) (screenHeight / xscale);
				break;
			case FIXED_HEIGHT:
				scale = yscale;
				GameConfig.STANDARD_WIDTH = (int) (screenWidth / yscale);
				break;
		}
		paintOffsetX = ((screenWidth - GameConfig.STANDARD_WIDTH * scale) / 2);
		paintOffsetY = ((screenHeight - GameConfig.STANDARD_HEIGHT * scale) / 2);
	}
}
