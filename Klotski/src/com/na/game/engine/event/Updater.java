package com.na.game.engine.event;

public interface Updater {
	public void update();
}
