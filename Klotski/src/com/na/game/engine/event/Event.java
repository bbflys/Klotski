package com.na.game.engine.event;

public abstract class Event {

	public static final int KEY_EVENT = 0;
	public static final int TOUCH_EVENT = 1;
	
	public int type;
	
	public Event(int type) {
		this.type = type;
	}
}
