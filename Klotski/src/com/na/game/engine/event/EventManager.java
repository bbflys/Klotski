package com.na.game.engine.event;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.na.game.engine.GameManager;
import com.na.game.engine.GameManager.Clazz;
import com.na.game.engine.event.impl.KeyEvent;
import com.na.game.engine.event.impl.TouchEvent;
import com.na.game.engine.ui.UI;
import com.na.game.engine.ui.UIManager;
import com.na.game.engine.widget.Widget;

public class EventManager implements Updater, Clazz {

	private Queue<Event> events = new ConcurrentLinkedQueue<Event>();
	private Widget focus;
	
	public void addEvent(Event event) {
		events.add(event);
	}
	
	public void setFocusWidget(Widget w) {
		focus = w;
	}
	
	public Widget getFocusWidget() {
		return focus;
	}
	
	@Override
	public void update() {
		while (!events.isEmpty()) {
			Event event = events.poll();
			if (event != null) {
				UI top = GameManager.get(UIManager.class).getTopUI();
				if (top != null) {
					switch (event.type) {
					case Event.KEY_EVENT:
						top.onKey((KeyEvent) event);
						break;
					case Event.TOUCH_EVENT:
						top.onTouch((TouchEvent) event);
						break;
					}
				}
			}
		}
	}

	@Override
	public Class<?> getClazz() {
		return getClass();
	}
}
