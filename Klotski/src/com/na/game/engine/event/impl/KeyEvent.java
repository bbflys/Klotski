package com.na.game.engine.event.impl;

import com.na.game.engine.event.Event;

public class KeyEvent extends Event {

	public int action, keyCode;
	
	public KeyEvent(int type, int action, int keyCode) {
		super(type);
		this.action = action;
		this.keyCode = keyCode;
	}

}
