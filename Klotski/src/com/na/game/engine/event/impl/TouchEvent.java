package com.na.game.engine.event.impl;

import com.na.game.engine.event.Event;

public class TouchEvent extends Event {

	public int action, x, y;
	
	public TouchEvent(int type, int action, int x, int y) {
		super(type);
		this.action = action;
		this.x = x;
		this.y = y;
	}

	public TouchEvent(int type, float action, float x, float y) {
		super(type);
		this.action = (int) action;
		this.x = (int) x;
		this.y = (int) y;
	}

}
