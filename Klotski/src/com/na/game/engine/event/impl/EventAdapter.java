package com.na.game.engine.event.impl;

import com.na.game.engine.event.EventListener;

public abstract class EventAdapter implements EventListener {

	@Override
	public boolean onKey(KeyEvent event) {
		return false;
	}

	@Override
	public boolean onTouch(TouchEvent event) {
		return false;
	}

}
