package com.na.game.engine.event;

import com.na.game.engine.event.impl.KeyEvent;
import com.na.game.engine.event.impl.TouchEvent;

public interface EventListener {
	public boolean onKey(KeyEvent event);
	public boolean onTouch(TouchEvent event);
}
