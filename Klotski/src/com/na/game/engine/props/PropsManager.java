package com.na.game.engine.props;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.content.Context;

import com.na.game.engine.GameManager;
import com.na.game.engine.GameManager.Clazz;
import com.na.game.engine.graphics.Image;
import com.na.game.engine.resource.ResourceManager;
import com.na.game.engine.util.GameConfig;
import com.na.game.engine.util.Util;

public class PropsManager implements Clazz {

//	private String desc;
	private int limit;
	private Map<String, GamePropsDef> propsDefs = new HashMap<String, GamePropsDef>();
	private Map<String, GameProps> gameProps = new HashMap<String, GameProps>();
	
	public PropsManager(Context context) {
		try {
			Document doc = Util.getDoc(context, GameConfig.RES_PROPS_XML);
			Element root = doc.getDocumentElement();
	//		desc = root.getAttribute("desc");
			limit = Integer.parseInt(root.getAttribute("limit"));
			NodeList nodes = root.getElementsByTagName("props");
			for (int i = 0; i < nodes.getLength(); i++) {
				Element node = (Element) nodes.item(i);
				String name = node.getAttribute("name");
				String desc = node.getAttribute("desc");
				String icon = node.getAttribute("icon");
				String price = node.getAttribute("price");
				String limit = node.getAttribute("limit");
				String clazz = node.getAttribute("class");
				GamePropsDef def = new GamePropsDef(name, desc, icon, price, limit, clazz);
				propsDefs.put(name, def);
			}
		} catch (Exception e) {}
	}
	
	public void addProps(String name, int amount) {
		GameProps gp = gameProps.get(name);
		if (gp != null) {
			gp.amount += amount;
		} else {
			GamePropsDef def = propsDefs.get(name);
			if (def == null) {
				throw new IllegalArgumentException("have no such props: " + name);
			}
			try {
				Class<GameProps> cls = (Class<GameProps>) Class.forName(def.clazz);
//				String name, String desc, int amount, Image icon, int price
				Constructor<GameProps> cnstcr = cls.getConstructor(String.class, String.class, int.class, Image.class, int.class);
				gp = cnstcr.newInstance(def.name, def.desc, amount, GameManager.get(ResourceManager.class).getImageFromAssets(def.icon), def.price);
				gameProps.put(name, gp);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean useProps(String name) {
		GameProps gp = gameProps.get(name);
		if (gp != null) {
			return gp.use();
		}
		return false;
	}
	
	public GamePropsDef[] getAllPropsDef() {
		return propsDefs.values().toArray(new GamePropsDef[propsDefs.size()]);
	}
	
	public GameProps[] getProps() {
		return gameProps.values().toArray(new GameProps[gameProps.size()]);
	}
	
	public int getLimit() {
		return limit;
	}
	
	/**
	 * clear props when game over
	 */
	public void clear() {
		gameProps.clear();
	}
	
	@Override
	public Class<?> getClazz() {
		return getClass();
	}
	
	public class GamePropsDef {
		private String name, desc, icon, clazz;
		private int price, limit;
		private GamePropsDef(String name, String desc, String icon, String price, String limit, String clazz) {
			this.name = name;
			this.desc = desc;
			this.icon = icon;
			this.clazz = clazz;
			try {
				this.price = Integer.parseInt(price);
				this.limit = Integer.parseInt(limit);
			} catch (Exception e) {
				this.price = 10;
				this.limit = 3;
			}
		}
		
		public String getName() {
			return name;
		}
		
		public String getDesc() {
			return desc;
		}
		
		public String getIcon() {
			return icon;
		}
		
		public String getClazz() {
			return clazz;
		}
		
		public int getPrice() {
			return price;
		}
		
		public int getLimit() {
			return limit;
		}
		
		@Override
		public String toString() {
			return "GamePropsDef [name: " + name + " , desc: " + desc + " , icon: " + icon + " , price: " + price + " , limit: " + limit + " , clazz: " + clazz + "]";
		}
	}
	
}
