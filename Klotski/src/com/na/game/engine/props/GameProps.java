package com.na.game.engine.props;

import com.na.game.engine.graphics.Image;

public abstract class GameProps {

	protected String name;
	protected String desc;
	protected int amount;
	protected Image icon;
	protected int price;
	
	protected Object extra;
	
	public GameProps(String name, String desc, int amount, Image icon, int price) {
		this.name = name;
		this.desc = desc;
		this.amount = amount;
		this.icon = icon;
		this.price = price;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public Image getIcon() {
		return icon;
	}
	
	public void setExtra(Object extra) {
		this.extra = extra;
	}
	
	public Object getExtra() {
		return extra;
	}
	
	protected boolean use() {
		if (amount > 0 && effect()) {
			amount--;
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "GP[name: " + name + " , desc: " + desc + " , amount: " + amount + "]";
	}
	
	protected abstract boolean effect();
	
}
