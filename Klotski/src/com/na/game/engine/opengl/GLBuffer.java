package com.na.game.engine.opengl;

import java.nio.Buffer;

public abstract class GLBuffer {

	protected int index; // index in GLBufferManager
	protected boolean recyclable;
	
	protected GLBuffer(int index) {
		this.index = index;
		recyclable = true; // default can recycle
	}
	
	protected void setRecyclable(boolean recyclable) {
		this.recyclable = recyclable;
	}
	
	protected boolean isRecyclable() {
		return recyclable;
	}
	
	protected void reset() {
		recyclable = false;
	}
	
	protected abstract Buffer getBuffer();
}
