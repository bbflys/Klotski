package com.na.game.engine.opengl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class GLFloatBuffer extends GLBuffer {

	protected IntBuffer intBuffer;
	protected FloatBuffer floatBuffer;
	
	protected GLFloatBuffer(int index, int capacity) {
		super(index);
		ByteBuffer bb = ByteBuffer.allocateDirect(capacity * 4).order(ByteOrder.nativeOrder());
		intBuffer = bb.asIntBuffer();
		floatBuffer = bb.asFloatBuffer();
	}
	
	protected void put(int[] src) {
		put(src, 0, src.length);
	}
	
	protected void put(int[] src, int off, int len) {
		intBuffer.put(src, off, len);
		intBuffer.position(0);
	}
	
	@Override
	public FloatBuffer getBuffer() {
		return floatBuffer;
	}

	@Override
	protected void reset() {
		super.reset();
		intBuffer.position(0);
		floatBuffer.position(0);
	}
}
