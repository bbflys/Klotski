package com.na.game.engine.opengl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

public class GLShortBuffer extends GLBuffer {

	protected ShortBuffer buffer;
	
	protected GLShortBuffer(int index, int capacity) {
		super(index);
		ByteBuffer bb = ByteBuffer.allocateDirect(capacity * 2).order(ByteOrder.nativeOrder());
		buffer = bb.asShortBuffer();
	}
	
	protected void put(short[] src) {
		buffer.put(src);
	}
	
	protected void put(short[] src, int off, int len) {
		buffer.put(src, off, len);
		buffer.position(0);
	}
	
	@Override
	protected ShortBuffer getBuffer() {
		return buffer;
	}

	@Override
	protected void reset() {
		super.reset();
		buffer.position(0);
	}
	
}
