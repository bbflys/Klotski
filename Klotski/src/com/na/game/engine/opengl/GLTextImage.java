package com.na.game.engine.opengl;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;

import com.na.game.engine.util.Util;

public class GLTextImage extends GLImage {

	protected GLTextImage() {
		this(null, null);
	}
	
	private GLTextImage(Bitmap bitmap, String name) {
		super(bitmap, name);
	}
	
	protected void drawText(String text, Paint paint, FontMetricsInt fmi) {
		String measureTxt = text.length() > 0 ? text : "测";
		int width = Util.getTextWidth(measureTxt, (int) paint.getTextSize());
		int height = fmi.bottom - fmi.top;//Util.getTextHeight(measureTxt, (int) paint.getTextSize());
		bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		if (text.length() > 0) {
			canvas.drawText(text, 0, -fmi.top, paint);
		}
		name = text;
	}

}
