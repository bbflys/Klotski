package com.na.game.klotski;

import android.graphics.Typeface;

import com.na.game.engine.GameActivity;
import com.na.game.engine.GameEntity;
import com.na.game.engine.ui.Resolution;
import com.na.game.engine.ui.UI;
import com.na.game.klotski.ui.InitUI;

public class Klotski extends GameEntity {

	private Typeface defaultFont;
	
	public Klotski(GameActivity context) {
		super(context);
	}

	@Override
	public UI getFirstUI() {
		return new InitUI();
	}

	@Override
	public Resolution getResolution() {
		return Resolution.FIXED_WIDTH;
	}

	@Override
	public Typeface getDefaultFont() {
		if (defaultFont == null) {
			defaultFont = Typeface.createFromAsset(context.getAssets(), "fonts/arial.ttf");
		}
		return defaultFont;
	}
	
}
