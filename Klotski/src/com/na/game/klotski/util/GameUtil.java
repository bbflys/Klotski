package com.na.game.klotski.util;

import com.na.game.klotski.entity.ItemType;

public class GameUtil {

	public static final int HOLD_TIME = 350; // millis
	
	public static final ItemType[] EXAMPLE_ITEM_TYPES = {
		ItemType.CLICK,
		ItemType.DOUBLE_CLICK,
		ItemType.HOLD,
		ItemType.TO_RIGHT,
		ItemType.REVERSE_RIGHT
	};
	public static final String[] HOW_TO_PLAY_TIPS = {
		"点一下任意地方",
		"快速点两下",
		"按住等一会儿",
		"向箭头方向滑动",
		"向箭头相反方向滑动"
	};
	
	public static final String GAME_SCORE_ID = "id_game_score";
	
//	public static void getGameScore(final GetObjListener<GameScore> listener) {
//		String objId = (String) GameManager.get(DataManager.class).get(GameUtil.GAME_SCORE_ID);
//		BmobQuery<GameScore> query = new BmobQuery<GameScore>();
//		query.setCachePolicy(CachePolicy.CACHE_ELSE_NETWORK);
//		query.setMaxCacheAge(24 * 60 * 60 * 1000L);
//		Context ctx = GameManager.get(GameActivity.class);
//		if (objId != null) {
//			query.getObject(ctx, objId, new GetListener<GameScore>() {
//				@Override
//				public void onFailure(int code, String msg) {
//					listener.onGet(null);
//				}
//				@Override
//				public void onSuccess(GameScore obj) {
//					listener.onGet(obj);
//				}
//			});
//		} else {
//			query.setLimit(1);
//			query.addWhereEqualTo("deviceId", Util.getDeviceId(ctx));
//			query.findObjects(ctx, new FindListener<GameScore>() {
//				@Override
//				public void onSuccess(List<GameScore> list) {
//					if (list != null && list.size() > 0) {
//						GameScore obj = list.get(0);
//						listener.onGet(obj);
//					}
//				}
//				@Override
//				public void onError(int code, String msg) {
//					listener.onGet(null);
//				}
//			});
//		}
//	}
//	
//	public static void updateGameScore(final int score) {
//		getGameScore(new GetObjListener<GameScore>() {
//			@Override
//			public void onGet(GameScore obj) {
//				if (obj == null) {
//					final GameScore gs = new GameScore(score, Util.getDeviceId(), Build.MANUFACTURER);
//					gs.save(GameManager.get(GameActivity.class), new SaveListener() {
//						@Override
//						public void onSuccess() {
//							GameManager.get(DataManager.class).put(GameUtil.GAME_SCORE_ID, gs.getObjectId());
//						}
//						@Override
//						public void onFailure(int code, String msg) {
//						}
//					});
//				} else {
//					obj.update(GameManager.get(GameActivity.class));
//				}
//			}
//		});
//	}
}
