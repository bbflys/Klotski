package com.na.game.klotski.ui;

import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.game.engine.GameManager;
import com.na.game.engine.data.DataManager;
import com.na.game.engine.event.impl.EventAdapter;
import com.na.game.engine.event.impl.TouchEvent;
import com.na.game.engine.graphics.Image;
import com.na.game.engine.resource.ResourceManager;
import com.na.game.engine.sound.SoundManager;
import com.na.game.engine.ui.UI;
import com.na.game.engine.ui.UIManager;
import com.na.game.engine.util.GameConfig;
import com.na.game.engine.util.Util;
import com.na.game.engine.widget.Layout;
import com.na.game.engine.widget.impl.Button;
import com.na.game.engine.widget.impl.Container;
import com.na.game.engine.widget.impl.Label;
import com.na.game.klotski.entity.Game;

public class GameEndUI extends UI {

	private Game game;
	
	public GameEndUI(Game game) {
		this.game = game;
	}
	
	@Override
	public void init() {
		DataManager dm = GameManager.get(DataManager.class);
		if (game.getScore() > dm.get("max_score", 0)) {
			dm.put("max_score", game.getScore());
		}
		Container ui = new Container();
//		ui.setBackgroundColor(0xaaf7cfb5);
		ui.setBound(0, 0, GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT);
		setWidget(ui);
		
		Label title = new Label("GAME OVER", 0xff4249b5);
		ui.addChild(title);
		title.setTextAlign(Layout.CENTER);
		title.setTextSize(80);
		title.setBound(0, 200, GameConfig.STANDARD_WIDTH, 200);
		
		int y = 400;
		int w = 400, h = 400;
		int x = (GameConfig.STANDARD_WIDTH - w) / 2;
		Container result = new Container();
		ui.addChild(result);
		result.setBound(x, y, w, h);
		result.setPainter(new BorderPainter(result, 4, 0xff77c298));
		
		// score
		Label lbl = new Label(String.valueOf(game.getScore()), 0xff4249b5);
		result.addChild(lbl);
		lbl.setBound(0, 0, result.getWidth(), result.getHeight());
		lbl.setTextAlign(Layout.CENTER);
		lbl.setTextSize(70);
		
		// the best
		lbl = new Label("最好  " + dm.get("max_score", 0), 0xff4249b5);
		result.addChild(lbl);
		lbl.setBound(0, result.getHeight() - 100, result.getWidth(), 100);
		lbl.setTextAlign(Layout.CENTER);
		lbl.setTextSize(40);
		
		// home
		ResourceManager rm = GameManager.get(ResourceManager.class);
		Image image = rm.getImageFromAssets("image/btn_quit.png");
		y += h + 20;
		x = 100;
		w = image.getWidth();
		h = image.getHeight();
		int gap = (ui.getWidth() - 3 * w - 2 * 100) / 2;
		Button home = new Button(image);
		ui.addChild(home);
		home.setImageColor(0xff77c298);
		home.setSrcRect(new Rect(0, 0, w, h));
		home.setBound(x, y, w, h);
		home.setEventListener(new BtnListener(0, home));
		
		// ranks
		x += w + gap;
		image = rm.getImageFromAssets("image/btn_score.png");
		Button ranks = new Button(image);
		ui.addChild(ranks);
		ranks.setImageColor(0xff77c298);
		ranks.setSrcRect(new Rect(0, 0, w, h));
		ranks.setBound(x, y, w, h);
		ranks.setEventListener(new BtnListener(1, ranks));
		
		// replay
		x += w + gap;
		image = rm.getImageFromAssets("image/btn_retry.png");
		Button replay = new Button(image);
		ui.addChild(replay);
		replay.setImageColor(0xff77c298);
		replay.setSrcRect(new Rect(0, 0, w, h));
		replay.setBound(x, y, w, h);
		replay.setEventListener(new BtnListener(2, replay));
	}
	
	class BtnListener extends EventAdapter {
		private int index;
		private Button owner;
		private Rect oldBound;
		private boolean valid;
		BtnListener(int index, Button owner) {
			this.index = index;
			this.owner = owner;
			oldBound = new Rect(owner.getBound());
		}
		@Override
		public boolean onTouch(TouchEvent event) {
			switch (event.action) {
				case MotionEvent.ACTION_DOWN:
					valid = true;
					owner.setScaled(true);
					Rect old = owner.getBound();
					oldBound.set(old);
					owner.setBound(old.left - 20, old.top - 20, old.width() + 40, old.height() + 40);
					GameManager.get(SoundManager.class).play("sounds/click.mp3");
					break;
				case MotionEvent.ACTION_UP:
					if (valid) {
						onClick();
					}
				case MotionEvent.ACTION_OUTSIDE:
					if (valid) {
						owner.setScaled(false);
						owner.setBound(oldBound);
						valid = false;
					}
					break;
			}
			return true;
		}
		private void onClick() {
			switch (index) {
				case 0: // home
					GameManager.get(UIManager.class).popUI();
					break;
				case 1: // ranks
					Util.showToast("此功能暂未开放");
					break;
				case 2: // replay
					GameManager.get(UIManager.class).popUI();
					GameManager.get(UIManager.class).pushUI(new GameUI());
					break;
			}
		}
	}

}
