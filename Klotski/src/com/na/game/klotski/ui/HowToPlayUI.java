package com.na.game.klotski.ui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.game.engine.GameManager;
import com.na.game.engine.event.impl.TouchEvent;
import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.graphics.Image;
import com.na.game.engine.graphics.Painter;
import com.na.game.engine.opengl.GLImage;
import com.na.game.engine.resource.ResourceManager;
import com.na.game.engine.sound.SoundManager;
import com.na.game.engine.ui.UI;
import com.na.game.engine.ui.UIManager;
import com.na.game.engine.util.GameConfig;
import com.na.game.engine.util.Timer;
import com.na.game.engine.util.TimerTask;
import com.na.game.engine.util.Util;
import com.na.game.engine.widget.Layout;
import com.na.game.engine.widget.impl.Container;
import com.na.game.engine.widget.impl.Icon;
import com.na.game.engine.widget.impl.Label;
import com.na.game.klotski.entity.Item;
import com.na.game.klotski.entity.ItemType;
import com.na.game.klotski.util.GameUtil;

public class HowToPlayUI extends UI {

	private Label tipLbl;
	private Icon[] icons;
	private int current;
	private int next;
	
	private static final int COLOR_CURRENT = 0xff77c298;
	private static final int COLOR_HIDE = 0x7f77c298;
	
	public HowToPlayUI() {
	}
	
	private class IconHoldPainter implements Painter {
		private Icon owner;
		private long startTime;
		private IconHoldPainter(Icon owner) {
			this.owner = owner;
			startTime = System.currentTimeMillis();
		}
		@Override
		public void paint(Graphics g) {
			int angle = (int) ((System.currentTimeMillis() - startTime) / (float) GameUtil.HOLD_TIME * 360);
			if (angle > 360) {
				angle = 360;
			}
			int x = owner.getAbsX() + owner.getWidth() / 2;
			int y = owner.getAbsY() + owner.getHeight() / 2;
			g.setColor(0x33000000);
			g.fillArc(x, y, owner.getWidth() / 2, owner.getHeight() / 2, 90, angle);
		}
	}
	
	@Override
	public void init() {
		Container ui = new Container();
		ui.setBound(0, 0, GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT);
		setWidget(ui);
		
		Label lbl = new Label(GameUtil.HOW_TO_PLAY_TIPS[0], Color.BLACK);
		ui.addChild(lbl);
		lbl.setTextSize(40);
		lbl.setTextAlign(Layout.CENTER);
		lbl.setBound(0, 0, GameConfig.STANDARD_WIDTH, 150);
		tipLbl = lbl;
		
		icons = new Icon[4];
		for (int i = 0; i < 4; i++) {
			Icon icon = new Icon(null);
			ui.addChild(icon);
			icon.setImageColor(i == 2 ? COLOR_CURRENT : COLOR_HIDE);
			icons[i] = icon;
		}
		
		// init
		for (int i = 0; i < 3; i++) {
			nextItem(true);
		}
		
//		// 1
//		Item item = getNextItem();
//		int w = item.getImage().getWidth() >> 2;
//		int h = item.getImage().getHeight() >> 2;
//		int x = GameConfig.STANDARD_WIDTH - w >> 1;
//		int y = 150;
//		y -= h / 2; // correct
//		Icon icon = icons[0];
//		icon.setIcon(item.getImage());
//		icon.setExtra(item);
//		icon.setBound(x, y, w, h);
//		icon.setScaled(true);
//		icon.setSrcRect(new Rect(0, 0, item.getImage().getWidth(), item.getImage().getHeight()));
//		icon.setImageColor(COLOR_HIDE);
//		
//		// 2
////		y += h + 80;
//		y = 300;
//		item = getNextItem();
//		w = item.getImage().getWidth() >> 1;
//		h = item.getImage().getHeight() >> 1;
//		x = GameConfig.STANDARD_WIDTH - w >> 1;
//		y -= h / 2; // correct
//		icon = icons[1];
//		icon.setIcon(item.getImage());
//		icon.setExtra(item);
//		icon.setBound(x, y, w, h);
//		icon.setScaled(true);
//		icon.setSrcRect(new Rect(0, 0, item.getImage().getWidth(), item.getImage().getHeight()));
//		icon.setImageColor(COLOR_HIDE);
//		
//		// 3
//		y = 600;
//		item = getNextItem();
//		w = item.getImage().getWidth();
//		h = item.getImage().getHeight();
//		x = GameConfig.STANDARD_WIDTH - w >> 1;
//		y -= h / 2; // correct
//		icon = icons[2];
//		icon.setIcon(item.getImage());
//		icon.setExtra(item);
//		icon.setBound(x, y, w, h);
//		icon.setScaled(true);
//		icon.setSrcRect(new Rect(0, 0, item.getImage().getWidth(), item.getImage().getHeight()));
//		icon.setImageColor(COLOR_CURRENT);
//		
//		// 4
//		y = 900;
//		w = 1;//item.getImage().getWidth() >> 2;
//		h = 1;//item.getImage().getHeight() >> 2;
//		x = GameConfig.STANDARD_WIDTH - w >> 1;
//		y -= h / 2; // correct
//		icon = icons[3];
//		icon.setBound(x, y, w, h);
//		icon.setScaled(true);
//		icon.setSrcRect(new Rect(0, 0, item.getImage().getWidth(), item.getImage().getHeight()));
//		icon.setImageColor(COLOR_HIDE);
	}
	
	private Item getNextItem() {
		int index = current++;//Util.RND.nextInt(ItemType.values().length);
		if (index >= GameUtil.EXAMPLE_ITEM_TYPES.length) {
			return null;
		}
		ItemType type = GameUtil.EXAMPLE_ITEM_TYPES[index];//ItemType.toType(index);
		String path = null;
		switch (type) {
			case CLICK:
				path = "image/click.png";
				break;
			case DOUBLE_CLICK:
				path = "image/click2.png";
				break;
			case HOLD:
				path = "image/hold.png";
				break;
			case TO_RIGHT:
				path = "image/swipe.png";
				break;
			case REVERSE_RIGHT:
				path = "image/swipe2.png";
				break;
		}
		if (path != null) {
			Image image = GameManager.get(ResourceManager.class).getImageFromAssets(path);
			if (GameConfig.useOpenGL) {
				((GLImage) image).setRecyclable(false);
			}
			return new Item(image, type);
		}
		int trans = Graphics.TRANS_NONE;
		path = "image/swipe2.png";
		switch (type) {
			case TO_UP:
				path = "image/swipe.png";
			case REVERSE_UP:
				trans = Graphics.TRANS_ROT270;
				break;
			case TO_DOWN:
				path = "image/swipe.png";
			case REVERSE_DOWN:
				trans = Graphics.TRANS_ROT90;
				break;
			case TO_LEFT:
				path = "image/swipe.png";
			case REVERSE_LEFT:
				trans = Graphics.TRANS_MIRROR;
				break;
		}
		Image src = GameManager.get(ResourceManager.class).getImageFromAssets(path);
		Bitmap bitmap = Bitmap.createBitmap(src.getBitmap(), 0, 0, src.getWidth(), src.getHeight(), Graphics.transformMatrix(trans), true);
		Image image = Util.wrapImage(bitmap);
		return new Item(image, type);
	}
	
	private void nextItem() {
		nextItem(false);
	}
	
	private void nextItem(boolean init) {
		if (!init) {
			tipLbl.setText(GameUtil.HOW_TO_PLAY_TIPS[++next]);
		}
		// move item
		for (int i = icons.length - 1; i > 0; i--) {
			Item item = (Item) icons[i - 1].getExtra();
			icons[i].setExtra(item);
			icons[i].setIcon(item != null ? item.getImage() : null);
		}
		Item item = getNextItem();
		icons[0].setExtra(item);
		icons[0].setIcon(item != null ? item.getImage() : null);
		// rebounds
		int x = 0, w = 0, h = 0;
		int[] y = { 150, 300, 600, 900 };
		for (int i = 0; i < icons.length; i++) {
			Icon icon = icons[i];
			icon.setPainter(null);
			item = (Item) icon.getExtra();
			if (item == null) {
				w = h = 1;
			} else if (i == 0 || i == 3) {
				w = item.getImage().getWidth() >> 2;
				h = item.getImage().getHeight() >> 2;
			} else if (i == 1) {
				w = item.getImage().getWidth() >> 1;
				h = item.getImage().getHeight() >> 1;
			} else {
				w = item.getImage().getWidth();
				h = item.getImage().getHeight();
			}
			x = GameConfig.STANDARD_WIDTH - w >> 1;
			y[i] -= h / 2; // correct
			icon.setBound(x, y[i], w, h);
			if (item != null) {
				icon.setScaled(true);
				icon.setSrcRect(new Rect(0, 0, item.getImage().getWidth(), item.getImage().getHeight()));
			}
		}
	}

	boolean pressed, dragged, dragging;
	long clickTime;
	int clickCount;
	int pressedX, pressedY;
	@Override
	public boolean onTouch(TouchEvent event) {
		switch (event.action) {
			case MotionEvent.ACTION_DOWN:
				pressed = true;
				dragged = false;
				dragging = false;
				pressedX = event.x;
				pressedY = event.y;
				if (clickTime != 0 && System.currentTimeMillis() - clickTime >= GameUtil.HOLD_TIME) {
					if (clickCount == 1 && getCurrentItem().getType() == ItemType.DOUBLE_CLICK) {
						playError();
						clickCount = 0;
					} else {
						clickCount++;
					}
				} else {
					clickCount++;
				}
				clickTime = System.currentTimeMillis();
				touchDown();
				break;
			case MotionEvent.ACTION_MOVE:
				if (dragging) {
					break;
				}
				int xoffset = event.x - pressedX;
				int yoffset = event.y - pressedY;
				int absx = Math.abs(xoffset);
				int absy = Math.abs(yoffset);
				if (absx > 20 || absy > 20) {
					dragged = true;
					dragging = true;
					if (absx > absy) { // horizontal
						if (xoffset > 0)
							moveToRight();
						else
							moveToLeft();
					} else {
						if (yoffset > 0)
							moveToDown();
						else
							moveToUp();
					}
				}
				break;
			case MotionEvent.ACTION_UP:
				if (dragged) {
					clickCount = 0;
				} else {
					long elapse = System.currentTimeMillis() - clickTime;
					if (elapse < GameUtil.HOLD_TIME) {
						if (clickCount == 1 && click()) {
							clickCount = 0;
						}
						if (clickCount == 2) {
							clickCount = 0;
							doubleClick();
						}
					} else {
						clickCount = 0;
						holdTouch();
					}
				}
				touchUp();
				clickTime = System.currentTimeMillis();
				break;
		}
		return true;
	}
	
	private void moveToUp() {
		if (getCurrentItem().getType() == ItemType.TO_UP || getCurrentItem().getType() == ItemType.REVERSE_UP) {
			nextItem();
			playCorrect();
		} else {
			playError();
		}
	}
	private void moveToDown() {
		if (getCurrentItem().getType() == ItemType.TO_DOWN || getCurrentItem().getType() == ItemType.REVERSE_DOWN) {
			nextItem();
			playCorrect();
		} else {
			playError();
		}
	}
	private void moveToLeft() {
		if (getCurrentItem().getType() == ItemType.TO_LEFT || getCurrentItem().getType() == ItemType.REVERSE_LEFT) {
			nextItem();
			playCorrect();
		} else {
			playError();
		}
	}
	private void moveToRight() {
		if (getCurrentItem().getType() == ItemType.REVERSE_RIGHT) { // end
			playCorrect();
			playEnd();
		} else if (getCurrentItem().getType() == ItemType.TO_RIGHT) {
			nextItem();
			playCorrect();
		} else {
			playError();
		}
	}
	private boolean click() {
		if (getCurrentItem().getType() == ItemType.CLICK) {
			nextItem();
			playCorrect();
			return true;
		}
		if (getCurrentItem().getType() != ItemType.DOUBLE_CLICK) {
			playError();
		}
		return false;
	}
	private void doubleClick() {
		if (getCurrentItem().getType() == ItemType.DOUBLE_CLICK) {
			nextItem();
			playCorrect();
		} else {
			playError();
		}
	}
	private void holdTouch() {
		if (getCurrentItem().getType() == ItemType.HOLD) {
			nextItem();
			playCorrect();
		} else {
			playError();
		}
	}
	
	private Item getCurrentItem() {
		return (Item) icons[2].getExtra();
	}
	
	private void playCorrect() {
		GameManager.get(SoundManager.class).play("sounds/correct.wav");
	}
	
	private void playError() {
		GameManager.get(SoundManager.class).play("sounds/error.m4a");
		if (getCurrentItem().getType() == ItemType.HOLD) {
			Icon icon = icons[2];
			icon.setPainter(null);
		}
	}
	
	private void playEnd() {
		GameManager.get(SoundManager.class).play("sounds/cheer.m4a");
		new Timer().schedule(new TimerTask() {
			@Override
			public void execute() {
				UIManager uiMngr = GameManager.get(UIManager.class);
				uiMngr.popUI();
				uiMngr.pushUI(new HomeUI());
			}
		}, 1000);
	}
	
	private void touchDown() {
		if (getCurrentItem().getType() == ItemType.HOLD) {
			Icon icon = icons[2];
			icon.setPainter(new IconHoldPainter(icon));
		}
	}
	
	private void touchUp() {
	}

	@Override
	public void update() {
		super.update();
	}
	
}
