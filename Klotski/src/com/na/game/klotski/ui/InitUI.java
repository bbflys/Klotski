package com.na.game.klotski.ui;

import android.graphics.Color;

import com.na.game.engine.GameActivity;
import com.na.game.engine.GameManager;
import com.na.game.engine.platform.PlatformApi;
import com.na.game.engine.platform.ad.Mediav;
import com.na.game.engine.ui.UI;
import com.na.game.engine.ui.UIManager;
import com.na.game.engine.util.GameConfig;
import com.na.game.engine.util.Timer;
import com.na.game.engine.util.TimerTask;
import com.na.game.engine.util.Util;
import com.na.game.engine.widget.impl.Container;
import com.na.game.engine.widget.impl.Label;

public class InitUI extends UI {

	@Override
	public void init() {
		Container ui = new Container();
		ui.setBackgroundColor(Color.BLACK);
		ui.setBound(0, 0, GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT);
		setWidget(ui);
		
		int w = Util.getTextWidth("别乱点", 150);
		int h = Util.getTextHeight("别", 150);
		int x = GameConfig.STANDARD_WIDTH - w >> 1;
		int y = GameConfig.STANDARD_HEIGHT - h >> 1;
		w /= 3;
		Label title = new Label("别", 0xff4249b5);
		ui.addChild(title);
		title.setTextSize(150);
		title.setBound(x, y, w, h);
		x += w;
		title = new Label("乱", 0xfff7c352);
		ui.addChild(title);
		title.setTextSize(150);
		title.setBound(x, y, w, h);
		x += w;
		title = new Label("点", 0xfff76963);
		ui.addChild(title);
		title.setTextSize(150);
		title.setBound(x, y, w, h);
		
		initData();
	}
	
	private void initData() {
		final GameActivity ctx = GameManager.get(GameActivity.class);
		ctx.runOnUiThread(new Runnable() {
			@Override
			public void run() {
//				BmobUtil.init();
//				PlatformApi.init(new WapsApi()); // 万普
//				PlatformApi.init(new Mobile7()); // mobile7
				PlatformApi.init(new Mediav());
				PlatformApi.invoke(PlatformApi.CMD_INIT);
				goHome();
			}
		});
	}
	
	private void goHome() {
		new Timer().schedule(new TimerTask() {
			@Override
			public void execute() {
				UIManager uiMngr = GameManager.get(UIManager.class);
				uiMngr.popUI();
				uiMngr.pushUI(new HomeUI());
			}
		}, 1000);
	}

}
