package com.na.game.klotski.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.game.engine.GameActivity;
import com.na.game.engine.GameMain;
import com.na.game.engine.GameManager;
import com.na.game.engine.data.DataManager;
import com.na.game.engine.event.impl.EventAdapter;
import com.na.game.engine.event.impl.KeyEvent;
import com.na.game.engine.event.impl.TouchEvent;
import com.na.game.engine.graphics.Image;
import com.na.game.engine.resource.ResourceManager;
import com.na.game.engine.sound.SoundManager;
import com.na.game.engine.ui.UI;
import com.na.game.engine.ui.UIManager;
import com.na.game.engine.util.GameConfig;
import com.na.game.engine.util.Util;
import com.na.game.engine.widget.impl.Button;
import com.na.game.engine.widget.impl.Container;
import com.na.game.engine.widget.impl.Label;

public class HomeUI extends UI {

	@Override
	public void init() {
		if (GameManager.get(DataManager.class).get("sound_on_off", 1) == 0) {
			GameManager.get(SoundManager.class).off();
		}
		
		Container ui = new Container();
//		ui.setBackgroundColor(0xaaf7cfb5);
		ui.setBound(0, 0, GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT);
		setWidget(ui);
		
		int w = Util.getTextWidth("别乱点", 150);
		int h = Util.getTextHeight("别", 150);
		int x = GameConfig.STANDARD_WIDTH - w >> 1;
		int y = 200;
		w /= 3;
		Label title = new Label("别", 0xff4249b5);
		ui.addChild(title);
		title.setTextSize(150);
		title.setBound(x, y, w, h);
		x += w;
		title = new Label("乱", 0xfff7c352);
		ui.addChild(title);
		title.setTextSize(150);
		title.setBound(x, y, w, h);
		x += w;
		title = new Label("点", 0xfff76963);
		ui.addChild(title);
		title.setTextSize(150);
		title.setBound(x, y, w, h);
		
		ResourceManager rm = GameManager.get(ResourceManager.class);
		
		Image[] images = new Image[]{
				rm.getImageFromAssets("image/menu.png"),
				rm.getImageFromAssets("image/menu2.png")
		};
		w = images[0].getWidth();
		h = images[0].getHeight();
		Button start = new Button(images, "开始", Color.WHITE);
		ui.addChild(start);
		start.setTextSize(40);
		start.setBound(GameConfig.STANDARD_WIDTH - w >> 1, GameConfig.STANDARD_HEIGHT - h >> 1, w, h);
		start.setEventListener(new EventAdapter() {
			@Override
			public boolean onTouch(TouchEvent event) {
				switch (event.action) {
					case MotionEvent.ACTION_DOWN:
						GameManager.get(SoundManager.class).play("sounds/click.mp3");
						break;
					case MotionEvent.ACTION_UP:
						GameManager.get(UIManager.class).pushUI(new GameUI());
						break;
				}
				return true;
			}
		});
		
		// ?
		x = 100;
		y = start.getY() + start.getHeight() + 50;
		Image image = rm.getImageFromAssets("image/ask.png");
		w = 120;
		h = 120;
		int gap = (ui.getWidth() - 3 * w - 2 * x) / 2;
		Button howtoplay = new Button(image);
		ui.addChild(howtoplay);
		howtoplay.setScaled(true);
		howtoplay.setSrcRect(new Rect(0, 0, image.getWidth(), image.getHeight()));
		howtoplay.setImageColor(Color.BLACK);
		howtoplay.setBound(x, y, w, h);
		howtoplay.setEventListener(new BtnListener(0, howtoplay));
		
		// ranks
		image = rm.getImageFromAssets("image/btn_score.png");
		x += w + gap;
		Button ranks = new Button(image);
		ui.addChild(ranks);
		ranks.setScaled(true);
		ranks.setSrcRect(new Rect(0, 0, image.getWidth(), image.getHeight()));
		ranks.setImageColor(Color.BLACK);
		ranks.setBound(x, y, w, h);
		ranks.setEventListener(new BtnListener(1, ranks));
		
		// sound on/off
		if (GameManager.get(DataManager.class).get("sound_on_off", 1) == 1) {
			image = rm.getImageFromAssets("image/sound_on.png");
		} else {
			image = rm.getImageFromAssets("image/sound_off.png");
		}
		x += w + gap;
		Button sound = new Button(image);
		ui.addChild(sound);
		sound.setScaled(true);
		sound.setSrcRect(new Rect(0, 0, image.getWidth(), image.getHeight()));
		sound.setImageColor(Color.BLACK);
		sound.setBound(x, y, w, h);
		sound.setEventListener(new BtnListener(2, sound));
	}

	@Override
	public boolean onKey(KeyEvent event) {
		if (event.action == android.view.KeyEvent.ACTION_UP && event.keyCode == android.view.KeyEvent.KEYCODE_BACK) {
			OnClickListener ok = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					GameManager.get(GameMain.class).stop();
				}
			};
			OnClickListener cancel = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			};
			final AlertDialog.Builder builder = new AlertDialog.Builder(GameManager.get(GameActivity.class))
										.setTitle("退出")
										.setMessage("确定退出游戏吗?")
										.setPositiveButton("确定", ok)
										.setNegativeButton("取消", cancel);
			GameManager.get(GameActivity.class).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					builder.show();
				}
			});
		}
		return true;
	}
	
	class BtnListener extends EventAdapter {
		private int index;
		private Button owner;
		private Rect oldBound;
		private boolean valid;
		BtnListener(int index, Button owner) {
			this.index = index;
			this.owner = owner;
			oldBound = new Rect(owner.getBound());
		}
		@Override
		public boolean onTouch(TouchEvent event) {
			switch (event.action) {
				case MotionEvent.ACTION_DOWN:
					valid = true;
					owner.setScaled(true);
					Rect old = owner.getBound();
					oldBound.set(old);
					owner.setBound(old.left - 20, old.top - 20, old.width() + 40, old.height() + 40);
					break;
				case MotionEvent.ACTION_UP:
					if (valid) {
						onClick();
					}
				case MotionEvent.ACTION_OUTSIDE:
					if (valid) {
						owner.setScaled(false);
						owner.setBound(oldBound);
						valid = false;
					}
					break;
			}
			return true;
		}
		private void onClick() {
			switch (index) {
				case 0: // ?
					GameManager.get(UIManager.class).pushUI(new HowToPlayUI());
					break;
				case 1: // ranks
					Util.showToast("此功能暂未开放");
					break;
				case 2: // sound
					int onoff = 1 - GameManager.get(DataManager.class).get("sound_on_off", 1);
					GameManager.get(DataManager.class).put("sound_on_off", onoff);
					if (onoff == 1) {
						Image image = GameManager.get(ResourceManager.class).getImageFromAssets("image/sound_on.png");
						owner.setImage(image, 0);
						GameManager.get(SoundManager.class).on();
					} else {
						Image image = GameManager.get(ResourceManager.class).getImageFromAssets("image/sound_off.png");
						owner.setImage(image, 0);
						GameManager.get(SoundManager.class).off();
					}
					break;
			}
			GameManager.get(SoundManager.class).play("sounds/click.mp3");
		}
	}
}
