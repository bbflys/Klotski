package com.na.game.klotski.ui;

import com.na.game.engine.graphics.Graphics;
import com.na.game.engine.graphics.Painter;
import com.na.game.engine.widget.Widget;

public class BorderPainter implements Painter {

	private Widget owner;
	private int borderSize;
	private int borderColor;
	
	public BorderPainter(Widget owner, int size, int color) {
		this.owner = owner;
		borderSize = size;
		borderColor = color;
	}
	
	@Override
	public void paint(Graphics g) {
		g.setColor(borderColor);
		int x = owner.getAbsX();
		int y = owner.getAbsY();
		int w = owner.getWidth();
		int h = owner.getHeight();
		g.setClip(x, y, w, h);
		// top
		g.fillRect(x, y, w, borderSize);
		// bottom
		g.fillRect(x, y + h - borderSize, w, borderSize);
		// left
		g.fillRect(x, y + borderSize, borderSize, h - 2 * borderSize);
		// right
		g.fillRect(x + w - borderSize, y + borderSize, borderSize, h - 2 * borderSize);
	}

}
