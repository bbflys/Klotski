package com.na.game.klotski.entity;

public enum ItemType {
	CLICK,
	DOUBLE_CLICK,
	HOLD,
	TO_UP, TO_DOWN, TO_LEFT, TO_RIGHT,
	REVERSE_UP, REVERSE_DOWN, REVERSE_LEFT, REVERSE_RIGHT;

	public static ItemType toType(int type) {
		for (ItemType it : values()) {
			if (type == it.ordinal()) {
				return it;
			}
		}
		return null;
	}
}
