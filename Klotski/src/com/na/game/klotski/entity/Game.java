package com.na.game.klotski.entity;

import com.na.game.engine.GameManager;
import com.na.game.engine.event.Updater;
import com.na.game.engine.sound.SoundManager;

public class Game implements Updater {

	private boolean end;
	private int score;
	private Item current;
	
	private static final int TIME_LIMIT = 30000;
	private long startTime;
	
	public Game() {
		startTime = System.currentTimeMillis();
	}
	
	@Override
	public void update() {
		if (System.currentTimeMillis() - startTime >= TIME_LIMIT) {
			end = true;
		}
	}
	
	public int getRemainSeconds() {
		int s = (int) ((TIME_LIMIT - (System.currentTimeMillis() - startTime)) / 1000);
		return s;
	}
	
	public boolean end() {
		return end;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setCurrent(Item item) {
		current = item;
	}
	
	public Item getCurrent() {
		return current;
	}
	
	private void playCorrect() {
		GameManager.get(SoundManager.class).play("sounds/correct.wav");
	}
	
	private void playError() {
		GameManager.get(SoundManager.class).play("sounds/error.m4a");
	}
	
	public boolean moveToUp() {
		if (current.getType() == ItemType.TO_UP || current.getType() == ItemType.REVERSE_UP) {
			score++;
			playCorrect();
			return true;
		}
		playError();
		end = true;
		return false;
	}
	
	public boolean moveToDown() {
		if (current.getType() == ItemType.TO_DOWN || current.getType() == ItemType.REVERSE_DOWN) {
			score++;
			playCorrect();
			return true;
		}
		playError();
		end = true;
		return false;
	}
	
	public boolean moveToLeft() {
		if (current.getType() == ItemType.TO_LEFT || current.getType() == ItemType.REVERSE_LEFT) {
			score++;
			playCorrect();
			return true;
		}
		playError();
		end = true;
		return false;
	}
	
	public boolean moveToRight() {
		if (current.getType() == ItemType.TO_RIGHT || current.getType() == ItemType.REVERSE_RIGHT) {
			score++;
			playCorrect();
			return true;
		}
		playError();
		end = true;
		return false;
	}
	
	public boolean click() {
		if (current.getType() == ItemType.CLICK) {
			score++;
			playCorrect();
			return true;
		}
		if (current.getType() != ItemType.DOUBLE_CLICK) {
			end = true;
			playError();
		}
		return false;
	}
	
	public boolean doubleClick() {
		if (current.getType() == ItemType.DOUBLE_CLICK) {
			score++;
			playCorrect();
			return true;
		}
		playError();
		end = true;
		return false;
	}
	
	public boolean checkDoubleClickError() {
		if (current.getType() == ItemType.DOUBLE_CLICK) {
			playError();
			end = true;
			return true;
		}
		return false;
	}
	
	public boolean holdTouch() {
		if (current.getType() == ItemType.HOLD) {
			score++;
			playCorrect();
			return true;
		}
		playError();
		end = true;
		return false;
	}

}
