//package com.na.game.klotski.entity;
//
//import cn.bmob.v3.BmobObject;
//
//public class GameScore extends BmobObject {
//
//	private static final long serialVersionUID = 1L;
//	
//	private int score;
//	private String deviceId;
//	private String name;
//
//	public GameScore() {}
//	
//	public GameScore(int score, String deviceId, String name) {
//		this.score = score;
//		this.deviceId = deviceId;
//		this.name = name;
//	}
//	
//	public int getScore() {
//		return score;
//	}
//
//	public void setScore(int score) {
//		this.score = score;
//	}
//
//	public String getDeviceId() {
//		return deviceId;
//	}
//
//	public void setDeviceId(String deviceId) {
//		this.deviceId = deviceId;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//}
