package com.na.game.klotski.entity;

import com.na.game.engine.graphics.Image;
import com.na.game.engine.resource.Destruction;

public class Item implements Destruction {

	private Image image;
	private ItemType type;
	
	public Item(Image image, ItemType type) {
		this.image = image;
		this.type = type;
	}
	
	public Image getImage() {
		return image;
	}
	
	public ItemType getType() {
		return type;
	}

	@Override
	public void destroy() {
		if (image != null) {
			image.destroy();
			image = null;
		}
	}
}
