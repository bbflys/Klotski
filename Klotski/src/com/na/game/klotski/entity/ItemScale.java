package com.na.game.klotski.entity;

public enum ItemScale {
	SMALL, NORMAL, BIG
}
